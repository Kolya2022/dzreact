// Core
import { combineReducers } from "redux";


// Reducers


import { ProductListReducer as ProductList } from "./reducers/ProductList";
import { favoriteReducer as favorite } from "./reducers/favorite";
import { modalReducer as modal } from "./reducers/modal";
import { basketReducer as basket } from "./reducers/basket";

export const rootReducer = combineReducers({
  ProductList,
  favorite,
  modal,
  basket,
});
