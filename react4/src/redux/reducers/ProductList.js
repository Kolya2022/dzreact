import { productTypes } from "../types";
const initialState = [];

export function ProductListReducer(state = initialState, action) {
  switch (action.type) {
    case productTypes.FILL_PRODUCT:
      return [...action.payload];

    default:
      return state;
  }
}
