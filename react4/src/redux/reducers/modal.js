import { modalTypes } from "../types";

const initialState = {
  isOpen:false,
  header:'',
  fnAction:()=>{},
};

export function modalReducer(state = initialState, action) {
  switch (action.type) {
    case modalTypes.SHOW_MODAL:
      return {
        ...action.payload,
        isOpen:true
      };
    case modalTypes.HIDE_MODAL:
      return {
        ...state,
        isOpen:false
      };
    default:
      return state;
  }
}
