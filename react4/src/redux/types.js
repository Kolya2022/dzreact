
export const productTypes = {
  FILL_PRODUCT: "FILL_PRODUCT",
};

export const favoriteTypes = {
  TOGGLE_FAV: "ToggleFav - TOGGLE_FAV",
  ADD_TO_FAVORITE:'AddToFavorite -  ADD_TO_FAVORITE',
  DELETE_FROM_FAVORITE:'AddToFavorite - DELETE_FROM_FAVORITE'
};

export const modalTypes = {
  SHOW_MODAL:'Modal - SHOW_MODAL',
  HIDE_MODAL:'Modal -  HIDE_MODAL'
}
export const basketTypes = {
    ADD_TO_BASKET:'Basket - ADD_TO_BASKET',
    DELETE_FROM_BASKET:'Basket - DELETE_FROM_BASKET'
}