import { favoriteTypes } from "../types";

export function addToFavorite(id) {
  return {
    type:favoriteTypes.ADD_TO_FAVORITE ,
    payload:  id ,
  };
}
export function deleteFromFavorite(id) {
  return {
    type: favoriteTypes.DELETE_FROM_FAVORITE,
    payload:  id ,
  };
}


export function addFavorite(payload) {
  return (dispatch, getState) => {
    dispatch(addToFavorite(payload));
    const { favorite } = getState();
    localStorage.setItem("Favorites", JSON.stringify(favorite.favorite));
  };
}

export function deleteFavorite(payload) {
  return (dispatch, getState) => {
    dispatch(deleteFromFavorite(payload));
    const { favorite } = getState();
    localStorage.setItem("Favorites", JSON.stringify(favorite.favorite));
  };
}