import { basketTypes } from "../types"


export function addToBasket(id){
    return{
        type:basketTypes.ADD_TO_BASKET,
        payload:id
    }
}
export function deleteFromBasket(id){
    return{
        type:basketTypes.DELETE_FROM_BASKET,
        payload:id
    }
}


export function addBasket(payload){
    return (dispatch, getState) => {
        dispatch(addToBasket(payload));
        const { basket } = getState();
        localStorage.setItem("Basket", JSON.stringify(basket.basket));
      };
}

export function deleteBasket(payload){
    return (dispatch, getState) => {
        dispatch(deleteFromBasket(payload));
        const { basket } = getState();
        localStorage.setItem("Basket", JSON.stringify(basket.basket));
      };
}