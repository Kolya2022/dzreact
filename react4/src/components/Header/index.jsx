import { Routes, Route, NavLink } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import { faHeart } from "@fortawesome/free-solid-svg-icons";

import './header.css'
export function Header() {
    const favorites = useSelector((state) =>  state.favorite.favorite);
    const basket = useSelector((state) => state.basket.basket);
  return (
    <>
   
    <header>
      <NavLink to="/">Home</NavLink>
      <NavLink to="/favorites">Favorites</NavLink>
      <NavLink to="/basket">Basket</NavLink>
      <div style={{ display: "flex" }}>
          <span style={{ color:"white" }}>
            <FontAwesomeIcon icon={faHeart} /> {favorites.length}
          </span>
          <span style={{ paddingLeft: "10px",color:"white" }}>
            <FontAwesomeIcon icon={faShoppingCart} /> {basket.length}
          </span>
        </div>
      </header>
    </>
  );
}
