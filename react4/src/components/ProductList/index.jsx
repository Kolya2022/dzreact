import ProductCard from "../ProductCard";
import Modal from "../Modal";
import "./productList.css";

export default function ProductList(props) {
  return (
    <>
      <Modal />
      <div className="product-list">
        {props.cardInfo.map((product) => (
          <ProductCard {...product} key={product.id} />
        ))}
      </div>
    </>
  );
}
