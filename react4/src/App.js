import "./App.css";

import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getProductAsync } from "./redux/actions/product";
import { Header } from "./components/Header";
import { AppRoutes } from "./Routes";

function App() {
  const dispatch = useDispatch();
  const cardInfo = useSelector((state) => state.ProductList);
  useEffect(() => {
    dispatch(getProductAsync());
  }, [dispatch]);

  return(
    <>
    <Header/>
    <AppRoutes cardInfo={cardInfo}/>
    </>
  )
  
}

export default App;
