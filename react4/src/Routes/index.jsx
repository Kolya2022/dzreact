

import { Routes, Route, NavLink } from "react-router-dom";
import { Home,Favorites,Basket } from "../Pages";



export function AppRoutes(props){

return(
    <>
    <Routes>
        <Route path="/" element={<Home cardInfo={props.cardInfo} />} />
        <Route path="/favorites" element={<Favorites cardInfo={props.cardInfo}/>} />
        <Route path="/basket" element={<Basket cardInfo={props.cardInfo}/>} />
      </Routes>

    </>
)
}