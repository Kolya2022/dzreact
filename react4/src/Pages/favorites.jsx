import ProductList from "../components/ProductList";
import { useSelector } from "react-redux";
import { useEffect, useState } from "react";

export function Favorites(props) {
  let { cardInfo } = props;
  const [cardInfoFavorites, setCardInfoFavorites] = useState([]);

  const favorites = useSelector((state) => state.favorite.favorite);

  useEffect(() => {
    const updatedCardInfoFavorites = cardInfo.filter((card) =>
      favorites.includes(card.id)
    );
    setCardInfoFavorites(updatedCardInfoFavorites);
  }, [favorites, cardInfo]);

  return (
    <>
      {cardInfoFavorites.length > 0 ? (
        <ProductList cardInfo={cardInfoFavorites} />
      ) : (
        <div>No favorites found</div>
      )}
    </>
  );
}
