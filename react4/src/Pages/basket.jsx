import ProductList from "../components/ProductList";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";

export function Basket(props) {
  let { cardInfo } = props;
  const [basketCard, setBasketCard] = useState([]);
  const basket = useSelector((state) => state.basket.basket);

  useEffect(() => {
    const updatedCardInfoFavorites = cardInfo.filter((card) =>
      basket.includes(card.id)
    );
    setBasketCard(updatedCardInfoFavorites);
  }, [basket, cardInfo]);

  return (
    <>
      {basketCard.length > 0 ? (
        <ProductList cardInfo={basketCard} />
      ) : (
        <div>No basket found</div>
      )}
    </>
  );
}
