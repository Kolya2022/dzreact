import ProductList from "../components/ProductList";

export function Home(props) {
  return (
    <>
      <ProductList cardInfo={props.cardInfo} />
    </>
  );
}
