
import * as Yup from "yup";








export const validationSchema = Yup.object({
  firstName: Yup.string()
    .min(2, "Minimum 2 letters")
    .max(20, "Maximum 20 letters")
    .matches(/^([A-Za-zа-яА-Я])+$/, 'Only letters')
    .required("This field is required"),
  lastName: Yup.string()
    .min(2, "Minimum 2 letters")
    .max(20, "Maximum 20 letters")
    .matches(/^([A-Za-zа-яА-Я])+$/, 'Only letters')
    .required("This field is required"),
  age: Yup.number()
    .min(18, "Minimum 18 years old")
    .max(99, "Maximum 99 years old")
    .required("This field is required"),
  address: Yup.string()
    .required("This field is required"),
  phoneNumber: Yup.string()
    .matches(/^\+?\d{10,12}$/, "Invalid phone number")
    .required("This field is required"),
});
