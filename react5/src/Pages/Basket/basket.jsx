import { useDispatch, useSelector } from "react-redux";
import { useState, useEffect } from "react";
import { ProductList } from "../../Components/ProductList";
import { Form } from "../../Components/Form";
import "./basket.css";

export function Basket({ cardInfo }) {
  const [cardInfoBasket, setCardIndoBasket] = useState([]);
  const basket = useSelector((state) => state.basket.basket);

  useEffect(() => {
    const cardBasket = cardInfo.filter((card) => basket.includes(card.id));
    setCardIndoBasket(cardBasket);
  }, [cardInfo, basket]);
  return (
    <>
      {cardInfoBasket.length > 0 ? (
        <div className="basket-wrapper">
          <div className="card-wrapper">
            {cardInfoBasket.map((card) => (
              <div key={card.id} className="card">
                <img src={card.img} alt="" height="120px" width="100px" />
                <div className="card__info" style={{ paddingLeft: "20px" }}>
                  <p className="card__name">{card.name}</p>
                  <p className="card__price">price:{card.price}</p>
                  <p className="card__articul">articul:{card.articul}</p>
                </div>
                <div className="card__cross"></div>
              </div>
            ))}
          </div>
          <Form cardInfoBasket={cardInfoBasket} />
        </div>
      ) : (
        <div>нет товаров в корзине</div>
      )}
    </>
  );
}
