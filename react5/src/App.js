import logo from "./logo.svg";
import "./App.css";
import { ProductList } from "./Components/ProductList";
import { useSelector, useDispatch } from "react-redux";
import { useEffect, useState } from "react";
import { getProductAsync } from "./redux/actions/product";
import { Header } from "./Components/Header";
import { AppRoutes } from "./Routes";
function App() {
  return (
    <>
      <Header />
      <AppRoutes />
    </>
  );
}

export default App;
