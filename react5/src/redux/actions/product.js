import { productTypes } from "../types";
export function fillProduct(data) {
  return {
    type: productTypes.FILL_PRODUCT,
    payload: data,
  };
}

export function getProductAsync() {
  return async function (dispatch) {
    const results = await fetch("../.././config.json").then((res) => res.json());
    dispatch(fillProduct(results));
  };
}
