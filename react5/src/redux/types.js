export const productTypes = {
  FILL_PRODUCT: "FILL_PRODUCT",
};

export const favoriteTypes = {
  ADD_FAVORITE: "AddFavorite - ADD_FAVORITE",
  DELETE_FAVORITE: "DELETEFavorite - DELETE_FAVORITE",
};
export const basketTypes = {
  ADD_TO_BASKET: "AddToBacket - ADD_TO_BASKET",
  DELETE_FROM_BASKET: "DeleteFromBasket - DELETE_FROM_BASKET",
};

export const modalTypes = {
  SHOW_MODAL: "ShowModal - SHOW_MODAL ",
  HIDE_MODAL: "HideModal - HIDE_MODAL",
};
