import { favoriteTypes } from "../types";

const initialState = {
  favorite: localStorage.getItem("Favorites")
    ? JSON.parse(localStorage.getItem("Favorites"))
    : [],
};

export function favoriteReducer(state = initialState, { type, payload }) {
  switch (type) {
    case favoriteTypes.ADD_FAVORITE:
      return { ...state, favorite: [...state.favorite, payload] };
    case favoriteTypes.DELETE_FAVORITE:
      const newFavorite = state.favorite.filter((id) => id !== payload);
      return { ...state, favorite: newFavorite };
    default:
      return state;
  }
}
