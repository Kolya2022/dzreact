import { useFormik } from "formik";
import "./form.css";
import * as Yup from "yup";
import { validationSchema } from "../../schemas";
import React from "react";
import { Input } from "../Input";
export function Form(props) {
  const formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      age: "",
      address: "",
      phoneNumber: "",
    },
    validationSchema,
    onSubmit: (values) => {
      console.log(values);
      console.log(props.cardInfoBasket);
    },
  });

  return (
    <form className="decor" onSubmit={formik.handleSubmit}>
      <div className="form-inner">
        <h3>Оформить заказ</h3>
        {Object.keys(formik.values).map((fieldName) => (
          <React.Fragment key={fieldName}>
            <Input
              type="text"
              id={fieldName}
              name={fieldName}
              placeholder={fieldName}
              value={formik.values[fieldName]}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
            {formik.touched[fieldName] && formik.errors[fieldName] && (
              <div style={{ paddingBottom: "10px", color: "#e9eff6" }}>
                {formik.errors[fieldName]}
              </div>
            )}
          </React.Fragment>
        ))}

        <input type="submit" value="Отправить" />
      </div>
    </form>
  );
}
