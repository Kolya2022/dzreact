import "./card.css";
import { useDispatch, useSelector } from "react-redux";
import { useState, useEffect } from "react";
import { addFavorite, deleteFavorite } from "../../redux/actions/favorite";
import { showModal, hideModal } from "../../redux/actions/modal";
import { addBasket, deleteBasket } from "../../redux/actions/basket";
export default function ProductCard(props) {
  const { name, price, color, articul, img, id } = props;
  const favorites = useSelector((state) => state.favorite.favorite);
  const basket = useSelector((state) => state.basket.basket);
  const dispatch = useDispatch();

  function toggleFav() {
    console.log(id);
    if (favorites.includes(id)) {
      dispatch(deleteFavorite(id));
    } else {
      dispatch(addFavorite(id));
    }
  }

  return (
    <>
      <div className="product-card ">
        <img className="product-card__img" src={img} alt="" />
        <p className="product-card__name">{name}</p>
        <p className="product-card__price">price:{price}</p>
        <p className="product-card__articul">articul:{articul}</p>
        <div className="product-card__bottons">
          {basket.includes(id) ? (
            <button
              onClick={() =>
                dispatch(
                  showModal({
                    title: "хотите удвлить товар из корзину?",
                    fnAction: () => dispatch(deleteBasket(id)),
                  })
                )
              }
            >
              {" "}
              delete from basket
            </button>
          ) : (
            <button
              onClick={() =>
                dispatch(
                  showModal({
                    title: "хотите добавить товар в корзину?",
                    fnAction: () => dispatch(addBasket(id)),
                  })
                )
              }
            >
              add to basket
            </button>
          )}

          <div
            id={id}
            onClick={() => toggleFav()}
            className={`product-card__heart ${
              favorites.includes(id) ? "clicked" : ""
            }`}
            style={{
              backgroundColor: favorites.includes(id) ? "red" : "yellow",
            }}
          ></div>
        </div>
      </div>
    </>
  );
}
