export function Input(props) {
  const { type, id, name, placeholder, value, onChange, onBlur } = props;

  return (
    <>
      <input
        type={type}
        id={id}
        name={name}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        onBlur={onBlur}
      />
    </>
  );
}
