import ProductCard from "../ProductCard";
import "./productList.css";
import Modal from "../Modal";
export function ProductList({ cardInfo, style }) {
  return (
    <>
      <Modal />
      <div className="card-list" style={style}>
        {cardInfo.map((card) => (
          <ProductCard {...card} key={card.id} />
        ))}
      </div>
    </>
  );
}
