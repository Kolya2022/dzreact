
import ProductList from "../Components/ProductList";

export function Home(props) {



    return (
        <>
            {props.cardInfo ? (
                <ProductList
                    cardInfo={props.cardInfo}
                    onChange={props.onChange}
                />
            ) : null}
        </>
    )

} 