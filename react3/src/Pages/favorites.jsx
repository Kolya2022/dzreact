import { useEffect, useState } from "react";

import ProductList from "../Components/ProductList";

export function Favorites(props) {
    const [cardInfo, setcardInfo] = useState([]);
    const [favorites, setFavorites] = useState([]);

    useEffect(() => {
        const favorites = localStorage.getItem("Favorites")
            ? JSON.parse(localStorage.getItem("Favorites"))
            : [];
        setFavorites(favorites);


        if (props.cardInfo && favorites.length > 0) {
            const productsInFavorites = props.cardInfo.filter((product) =>
                favorites.includes(product.id)
            );
            setcardInfo(productsInFavorites);
        } else {
            setcardInfo([]);
        }


    }, [props]);

    return (
        <>
            {favorites.length === 0 ? (
                <div>Нет товаров</div>
            ) : null}
            {props.cardInfo ? (
                <ProductList
                    cardInfo={cardInfo}
                    onChange={props.onChange}

                />
            ) : null}
        </>
    );
}
