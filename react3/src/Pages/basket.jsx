import ProductList from "../Components/ProductList";
import { useEffect, useState } from "react";
import PropTypes from "prop-types";

export function Basket(props) {
  const [cardInfo, setcardInfo] = useState([]);
  const [basket, setbasket] = useState([]);
  const [inBasketbasket, setinBasketbasket] = useState(true);

  useEffect(() => {
    const basket = localStorage.getItem("basket")
      ? JSON.parse(localStorage.getItem("basket"))
      : [];
    setbasket(basket);
    if (props.cardInfo && basket) {
      const productsInbasket = props.cardInfo.filter((product) =>
        basket.includes(product.id)
      );

      setcardInfo(productsInbasket);
    }
  }, [props]);

  return (
    <>
      <main>
        <section>
          {basket.length === 0 ? <div>Корзина пустая</div> : null}
          {props.cardInfo && (
            <ProductList
              cardInfo={cardInfo}
              onChange={props.onChange}
              basket={props.basket}
              inBasketbasket={inBasketbasket}
            />
          )}
        </section>
      </main>
    </>
  );
}

Basket.propTypes = {
  productList: PropTypes.arrayOf(PropTypes.object),
  onChange: PropTypes.func,
};
