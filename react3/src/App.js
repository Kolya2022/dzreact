import "./App.css";
import { useState, useEffect } from "react";
import { Home, Favorites, Basket } from "./Pages";
import { Routes, Route, NavLink } from "react-router-dom";
import { useFetch } from "./hooks";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import { faHeart } from "@fortawesome/free-solid-svg-icons";

function App() {
  const cardInfo = useFetch("./config.json");
  const [Storage, setStorage] = useState(false);
  const [favorites, setFavorites] = useState([]);
  const [basket, setbasket] = useState([]);

  useEffect(() => {
    const favorites = localStorage.getItem("Favorites")
      ? JSON.parse(localStorage.getItem("Favorites"))
      : [];
    setFavorites(favorites);
    const basket = localStorage.getItem("basket")
      ? JSON.parse(localStorage.getItem("basket"))
      : [];
    setbasket(basket);
  }, [Storage]);
  const onChange = () => {
    setStorage((prevState) => !prevState);
  };

  return (
    <>
      <header>
        <NavLink to="/">Home</NavLink>
        <NavLink to="/favorites">Favorites</NavLink>
        <NavLink to="/basket">Basket</NavLink>
        <div style={{ display: "flex" }}>
          <span>
            <FontAwesomeIcon icon={faHeart} /> {favorites.length}
          </span>
          <span style={{ paddingLeft: "10px" }}>
            <FontAwesomeIcon icon={faShoppingCart} /> {basket.length}
          </span>
        </div>
      </header>
      <Routes>
        <Route
          path="/"
          element={<Home cardInfo={cardInfo.data} onChange={onChange} />}
        />
        <Route
          path="/favorites"
          element={<Favorites cardInfo={cardInfo.data} onChange={onChange} />}
        />
        <Route
          path="/basket"
          element={
            <Basket
              cardInfo={cardInfo.data}
              onChange={onChange}
              basket={basket}
            />
          }
        />
      </Routes>
    </>
  );
}

export default App;
