import { useEffect, useState } from "react";
import ProductCard from "../ProductCard";
import "./productList.css";

export default function ProductList(props) {
  const [favorites, setFavorites] = useState([]);
  const [basket, setbasket] = useState([]);
  useEffect(() => {
    const basket = localStorage.getItem("basket")
      ? JSON.parse(localStorage.getItem("basket"))
      : [];
    setbasket(basket);
    const favorites = localStorage.getItem("Favorites")
      ? JSON.parse(localStorage.getItem("Favorites"))
      : [];
    setFavorites(favorites);
  }, [props]);

  const addToFavorites = (id) => {
    if (!favorites.includes(id)) {
      const newFavorites = [...favorites, id];
      localStorage.setItem("Favorites", JSON.stringify(newFavorites));
      setFavorites(newFavorites);
    }
  };

  const deleteFromFavorites = (id) => {
    const newFavorites = favorites.filter((product) => product !== id);
    localStorage.setItem("Favorites", JSON.stringify(newFavorites));
    setFavorites(newFavorites);
  };

  const addTobasket = (id) => {
    if (!basket.includes(id)) {
      const newbasket = [...basket, id];
      localStorage.setItem("basket", JSON.stringify(newbasket));
    }
  };

  const removeFromBasket = (id) => {
    const newbasket = basket.filter((product) => product !== id);
    localStorage.setItem("basket", JSON.stringify(newbasket));
  };

  return (
    <div className="product-list">
      {props.cardInfo.map((product) => (
        <ProductCard
          {...product}
          isFavorite={favorites.includes(product.id)}
          inbasket={basket.includes(product.id)}
          inBasketbasket={props.inBasketbasket}
          basket={props.basket}
          addToFavorites={addToFavorites}
          deleteFromFavorites={deleteFromFavorites}
          onChange={props.onChange}
          addTobasket={addTobasket}
          removeFromBasket={removeFromBasket}
        />
      ))}
    </div>
  );
}
