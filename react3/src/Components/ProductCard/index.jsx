import Button from "../Button";
import { useState, useEffect } from "react";
import configModal from "../Modal/configModal";
import "./card.css";
import Modal from "../Modal";

export default function ProductCard(props) {
  const [isOpen, SetIsOpen] = useState(false);
  const [modalInfo, SetModalInfo] = useState([]);
  const [isFavorite, setIsFavorite] = useState(false);
  const [inbasket, setInbasket] = useState(false);
  useEffect(() => {
    setInbasket(props.inbasket);
    setIsFavorite(props.isFavorite);
  }, [props]);

  const setIsInbasket = () => {
    inbasket ? props.removeFromBasket(props.id) : props.addTobasket(props.id);
    props.addTobasket(props.id);
    setInbasket((prevSate) => !prevSate);
    SetIsOpen((prev) => !prev);
    props.onChange();
  };
  const openModal = (e) => {
    const modalId = e.target.id;
    const modalInfo = configModal.find((modal) => modal.id === modalId);
    const updatedmodalInfo = {
      ...modalInfo,
      actions: [
        <Button
          text="ок"
          backgroundColor="red"
          className="button"
          onClick={() => setIsInbasket()}
        />,
        <Button
          text="Cancel"
          backgroundColor="red"
          className="button btn-close"
        />,
      ],
    };
    SetModalInfo((prev) => updatedmodalInfo);
    SetIsOpen((prev) => !prev);
  };
  const closeModal = (e) => {
    console.log(e.target.classList);
    if (e.target.classList.contains("modal__btn-close")) {
      SetIsOpen((prev) => !prev);
    }
  };
  const setIsInFavorite = (id) => {
    isFavorite ? props.deleteFromFavorites(id) : props.addToFavorites(id);

    setIsFavorite((prevState) => !prevState);
    props.onChange();
  };

  const { name, price, color, articul, img, id } = props;

  return (
    <>
      {isOpen ? <Modal {...modalInfo} closeModal={closeModal} /> : null}
      <div
        className="product-card"
        onClick={closeModal}
        style={{ backgroundColor: color }}
      >
        {props.inBasketbasket ? (
          <div
            id="0"
            onClick={(e) => openModal(e)}
            class="product-card__cross"
          ></div>
        ) : null}
        <img className="product-card__img" src={img} alt="" />
        <p className="product-card__name">{name}</p>
        <p className="product-card__price">price:{price}</p>
        <p className="product-card__articul">articul:{articul}</p>
        <div className="product-card__bottons">
          {inbasket ? (
            <div>товар в корзине </div>
          ) : (
            <Button
              id="1"
              backgroundColor="white"
              text="Add to basket"
              onClick={(e) => openModal(e)}
              className="button"
            />
          )}

          <div
            id={id}
            onClick={() => setIsInFavorite(id)}
            className={`product-card__heart ${isFavorite ? "clicked" : ""}`}
            style={{ backgroundColor: isFavorite ? "red" : "yellow" }}
          ></div>
        </div>
      </div>
    </>
  );
}
