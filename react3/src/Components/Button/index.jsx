import "./button.css";

export default function Button(props) {
  const { text, backgroundColor, id, className, onClick } = props;
  return (
    <button
      id={id}
      onClick={onClick}
      className={className}
      style={{ backgroundColor }}
    >
      {text}
    </button>
  );
}
