import React from "react";
const configModal = [
  {
    id: "0",
    header: "Хотите удалить товар из корзины?",
    text: "Нажмите 'Ок' чтобы продолжить",
    closeButton: true,
    modalRef: React.createRef(),
    actions: [],
  },
  {
    id: "1",
    header: "Хотите добавиь товар в корзину?",
    text: `Нажмите "ок" чтобы добавить`,
    closeButton: true,
    modalRef: React.createRef(),
    actions: [],
  },
];

export default configModal;
