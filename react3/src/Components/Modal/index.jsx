import "./modal.css";

export default function Modal(props) {
  const {
    text,
    id,
    header,
    actions,
    closeButton,
    modalRef,
    closeModal,
    onAddTobasket,
  } = props;

  return (
    <div id={id} className="modal">
      <div className="modal__content" ref={modalRef} onClick={onAddTobasket}>
        <div className="modal__header">
          {header}{" "}
          {closeButton ? (
            <span className="modal__btn-close" onClick={closeModal}>
              &#10761;
            </span>
          ) : null}
        </div>
        <div className="modal__body">
          <div>{text}</div>
          <div className="modal__btn">{actions}</div>
        </div>
      </div>
    </div>
  );
}
