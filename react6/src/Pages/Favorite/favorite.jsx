import { useDispatch, useSelector } from "react-redux";
import { useState, useEffect } from "react";
import { ProductList } from "../../Componens/ProductList";

export function Favorites({ cardInfo }) {
  const [cardInfoFavorites, setCardInfoFavorites] = useState([]);

  const favorite = useSelector((state) => state.favorite.favorite);
  useEffect(() => {
    const favoriteCard = cardInfo.filter((card) => favorite.includes(card.id));
    setCardInfoFavorites(favoriteCard);
  }, [favorite, cardInfo]);

  return (
    <>
      {cardInfoFavorites.length > 0 ? (
        <ProductList cardInfo={cardInfoFavorites} />
      ) : (
        <div>No favorites</div>
      )}
    </>
  );
}
