import { useDispatch, useSelector } from "react-redux";
import { getProductAsync } from "../../redux/actions/product";
import { useEffect } from "react";

import { ProductList } from "../../Componens/ProductList";

export function Home({ cardInfo }) {
  return (
    <>
      <ProductList cardInfo={cardInfo} />
    </>
  );
}
