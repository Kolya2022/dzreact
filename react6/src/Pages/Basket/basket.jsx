import { useDispatch, useSelector } from "react-redux";
import { useState, useEffect } from "react";
import { ProductList } from "../../Componens/ProductList";
import { Form } from "../../Componens/Form";
import "./basket.css";
import { deleteBasket } from "../../redux/actions/basket";

export function Basket({ cardInfo }) {
  const [cardInfoBasket, setCardIndoBasket] = useState([]);
  const basket = useSelector((state) => state.basket.basket);
  const dispatch = useDispatch()
  useEffect(() => {
    const cardBasket = cardInfo.filter((card) => basket.includes(card.id));
    setCardIndoBasket(cardBasket);
  }, [cardInfo, basket]);
  return (
    <>
      {cardInfoBasket.length > 0 ? (
        <div className="basket-wrapper">
          <div className="card-wrapper">
            {cardInfoBasket.map((card) => (
              <div key={card.id} className="card-basket">
                <img src={card.img} alt="" height="120px" width="100px" />
                <div className="card-basket__info" style={{ paddingLeft: "20px" }}>
                  <p className="card-basket__name">{card.name}</p>
                  <p className="card-basket__price">price:{card.price}</p>
                  <p className="card-basket__articul">articul:{card.articul}</p>
                </div>
                <div className="card-basket__cross" onClick={()=>dispatch(deleteBasket(card.id))}></div>
              </div>
            ))}
          </div>
          <Form cardInfoBasket={cardInfoBasket} />
        </div>
      ) : (
        <div>нет товаров в корзине</div>
      )}
    </>
  );
}
