import { Routes, Route, NavLink } from "react-router-dom";
import { Home, Favorites, Basket } from "../Pages";

import { useDispatch, useSelector } from "react-redux";
import { getProductAsync } from "../redux/actions/product";
import { useEffect } from "react";

export function AppRoutes(props) {
  const dispatch = useDispatch();
  const cardInfo = useSelector((state) => state.ProductList);
  useEffect(() => {
    dispatch(getProductAsync());
  }, [dispatch]);
  return (
    <>
      <Routes>
        <Route path="/" element={<Home cardInfo={cardInfo} />} />
        <Route path="/favorites" element={<Favorites cardInfo={cardInfo} />} />
        <Route path="/basket" element={<Basket cardInfo={cardInfo} />} />
      </Routes>
    </>
  );
}
