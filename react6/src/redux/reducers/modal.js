import { modalTypes } from "../types";

const initialState = {
  isOpen: false,
  title: "",
  fnAction: () => {},
};

export function modalReducer(state = initialState, { payload, type }) {
  switch (type) {
    case modalTypes.SHOW_MODAL:
      return { ...payload, isOpen: true };

    case modalTypes.HIDE_MODAL:
      return { ...payload, isOpen: false };
    default:
      return state;
  }
}
