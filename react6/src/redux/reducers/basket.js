import { basketTypes } from "../types";

const initialState = {
  basket: localStorage.getItem("Basket")
    ? JSON.parse(localStorage.getItem("Basket"))
    : [],
};

export function basketReducer(state = initialState, { type, payload }) {
  switch (type) {
    case basketTypes.ADD_TO_BASKET:
      return { ...state, basket: [...state.basket, payload] };
    case basketTypes.DELETE_FROM_BASKET:
      const updateBasket = state.basket.filter((basketId) => basketId !== payload);
      return { ...state, basket: updateBasket };
      default : 
      return state
  }
}
