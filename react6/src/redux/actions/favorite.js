import { favoriteTypes } from "../types"




export function deleteFav(id){
    return{
    type:favoriteTypes.DELETE_FAVORITE,
    payload:id
    }
}

export function addFav(id){
    return{
        type:favoriteTypes.ADD_FAVORITE,
        payload:id
    }
}



export function addFavorite(payload) {
  return (dispatch, getState) => {
    dispatch(addFav(payload));
    const { favorite } = getState();
    localStorage.setItem("Favorites", JSON.stringify(favorite.favorite));
  };
}

export function deleteFavorite(payload) {
  return (dispatch, getState) => {
    dispatch(deleteFav(payload));
    const { favorite } = getState();
    localStorage.setItem("Favorites", JSON.stringify(favorite.favorite));
  };
}
