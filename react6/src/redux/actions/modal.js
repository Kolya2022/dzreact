import { modalTypes } from "../types";

export function showModal(body){
    return {
        type: modalTypes.SHOW_MODAL,
        payload:body
    }
}
export function hideModal(){
    return {
        type: modalTypes.HIDE_MODAL,
    }
}