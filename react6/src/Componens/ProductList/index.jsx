import ProductCard from "../ProductCard";
import "./productList.css";
import Modal from "../Modal";
import { Button } from "../Button";
import { useState, useContext } from "react";
import { toggleListContext } from "../../context/toggleListContex";
export function ProductList({ cardInfo, style }) {
  // const [toggelist,setToggelist] = useState(false)
  const { toggleList, setToggleList } = useContext(toggleListContext);
  function changeList() {
    setToggleList(!toggleList);
  }
  return (
    <>
      <Modal />
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-evenly",
        }}
      >
        <h2>Детские игрушки</h2>
        <Button text="change card" onClick={changeList} />
      </div>

      <div className="card-list" style={style}>
        {cardInfo.map((card) => (
          <ProductCard {...card} key={card.id} toggleListContext={toggleList} />
        ))}
      </div>
    </>
  );
}
