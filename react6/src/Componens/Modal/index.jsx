import "./modal.css"
import { Button } from "../Button";
import { useDispatch, useSelector } from "react-redux";
import { showModal, hideModal } from "../../redux/actions/modal";

export default function Modal(props) {
const dispatch = useDispatch();

  const modal = useSelector(state => state.modal);

  if (!modal.isOpen) return null;


    const { text, id,  actions, closeButton,} = props
    const {title,fnAction} = modal


    return (
        <div data-testid='Modal' id={id} className="modal" >
            <div className="modal__content" >
                <div className="modal__header">{title} {closeButton ? <span className={`btn-close `} >&#10761;</span> : null}</div>
                <div className="modal__body">
                    <div>нажмине ок чтобы продолжить</div>
                    <div className="modal__buttons">
                    <Button onClick={()=>{dispatch(hideModal());fnAction()}} text='Ok' color='red'/>
                    <Button onClick={()=>{dispatch(hideModal());fnAction()}} text='Cancel'color='red'/>
                    </div>
                    
                </div>
                
            </div>
        </div>
    )
}