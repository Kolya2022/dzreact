import "./card.css";
import { useDispatch, useSelector } from "react-redux";
import { useState, useEffect, useContext } from "react";
import { addFavorite, deleteFavorite } from "../../redux/actions/favorite";
import { showModal, hideModal } from "../../redux/actions/modal";
import { addBasket, deleteBasket } from "../../redux/actions/basket";
import { toggleListContext } from "../../context/toggleListContex";
import { Button } from "../Button";
export default function ProductCard(props) {
  const { name, price, color, articul, img, id, toggleListContext } = props;
  const favorites = useSelector((state) => state.favorite.favorite);
  const basket = useSelector((state) => state.basket.basket);
  const dispatch = useDispatch();

  function toggleFav() {
    console.log(id);
    if (favorites.includes(id)) {
      dispatch(deleteFavorite(id));
    } else {
      dispatch(addFavorite(id));
    }
  }

  return (
    <>
      <div className={toggleListContext ? "card--list" : "card--grid"}>
        <img className="card__img" src={img} alt="" />
        <div
          className={
            toggleListContext ? " card-content--list" : " card-content--grid"
          }
        >
          <p className="card__name">{name}</p>
          <p className="card__price">price:{price}</p>
          <p className="card__articul">articul:{articul}</p>
          <div className="card__bottons">
            {basket.includes(id) ? (
              <Button
                onClick={() =>
                  dispatch(
                    showModal({
                      title: "хотите удвлить товар из корзину?",
                      fnAction: () => dispatch(deleteBasket(id)),
                    })
                  )
                }
                text="delete from basket"
              />
            ) : (
              <Button
                onClick={() =>
                  dispatch(
                    showModal({
                      title: "хотите добавить товар в корзину?",
                      fnAction: () => dispatch(addBasket(id)),
                    })
                  )
                }
                text="add to basket"
              />
            )}

            <div
              id={id}
              onClick={() => toggleFav()}
              className={`card__heart ${
                favorites.includes(id) ? "clicked" : ""
              }`}
              style={{
                backgroundColor: favorites.includes(id) ? "red" : "yellow",
              }}
            ></div>
          </div>
        </div>
      </div>
    </>
  );
}
