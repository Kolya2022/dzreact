export function Button(props) {
  const { color, onClick, text, className } = props;

  return (
    <button
      data-testid='buttonTest'
      className={className}
      onClick={onClick}
      style={{ backgroundColor: color }}
    >
      {text}
    </button>
  );
}
