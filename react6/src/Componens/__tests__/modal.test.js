import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import Modal from "../Modal";
import { showModal, hideModal } from "../../redux/actions/modal";

const mockStore = configureMockStore();

test("renders modal when modal.isOpen is true", () => {
  const initialState = { modal: { isOpen: true, title: "Test Modal" } };
  const store = mockStore(initialState);
  render(
    <Provider store={store}>
      <Modal text="Modal Body" id="testModal" />
    </Provider>
  );

  const modalElement = screen.getByTestId("Modal");
  expect(modalElement).toBeInTheDocument();
  expect(modalElement).toHaveAttribute("id", "testModal");
  expect(screen.getByText("Ok")).toBeInTheDocument();
  expect(screen.getByText("Cancel")).toBeInTheDocument();
});

test("does not render modal when modal.isOpen is false", () => {
  const initialState = { modal: { isOpen: false } };
  const store = mockStore(initialState);
  render(
    <Provider store={store}>
      <Modal text="Modal Body" />
    </Provider>
  );

  const modalElement = screen.queryByTestId("Modal");
  expect(modalElement).toBeNull();
});

test("calls fnAction when 'Ok' button is clicked", () => {
  const fnActionMock = jest.fn();
  const initialState = {
    modal: { isOpen: true, title: "Test Modal", fnAction: fnActionMock },
  };
  const store = mockStore(initialState);
  render(
    <Provider store={store}>
      <Modal text="Modal Body" />
    </Provider>
  );

  const okButton = screen.getByText("Ok");
  fireEvent.click(okButton);

  expect(fnActionMock).toHaveBeenCalledTimes(1);
});

test("calls dispatch(hideModal()) when 'Cancel' button is clicked", () => {
  const dispatchMock = jest.fn();
  const initialState = { modal: { isOpen: true, title: "Test Modal" } };
  const store = mockStore(initialState);
  render(
    <Provider store={store}>
      <Modal text="Modal Body" />
    </Provider>
  );

  const cancelButton = screen.getByText("Cancel");
  fireEvent.click(cancelButton);

  const expectedAction = hideModal();
  expect(store.getActions()).toContainEqual(expectedAction);
});
