import { basketReducer } from "../../redux/reducers/basket";
import { basketTypes } from "../../redux/types";
describe("basketReducer", () => {
  it("should return initial state", () => {
    const initialState = {
      basket: [],
    };
    const action = { type: "INVALID_ACTION" };
    const newState = basketReducer(undefined, action);
    expect(newState).toEqual(initialState);
  });

  it("should handle ADD_TO_BASKET", () => {
    const initialState = {
      basket: [1, 2, 3],
    };
    const payload = 4;
    const action = { type: basketTypes.ADD_TO_BASKET, payload };
    const newState = basketReducer(initialState, action);
    expect(newState.basket).toEqual([...initialState.basket, payload]);
  });

  it("should handle DELETE_FROM_BASKET", () => {
    const initialState = {
      basket: [1, 2, 3],
    };
    const payload = 2;
    const action = { type: basketTypes.DELETE_FROM_BASKET, payload };
    const newState = basketReducer(initialState, action);
    expect(newState.basket).toEqual([1, 3]);
  });

  it("should return state as is for unknown action types", () => {
    const initialState = {
      basket: [1, 2, 3],
    };
    const action = { type: "SOME_UNKNOWN_ACTION" };
    const newState = basketReducer(initialState, action);
    expect(newState).toEqual(initialState);
  });
});
