

import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import { Button } from "../Button";

test("renders button with provided text", () => {
  const buttonText = "Click me!";
  render(<Button text={buttonText} />);
  const buttonElement = screen.getByTestId("buttonTest");
  expect(buttonElement).toBeInTheDocument();
  expect(buttonElement).toHaveTextContent(buttonText);
});

// Тест для проверки класса кнопки
test("button has a given className", () => {
  const className = "custom-button";
  render(<Button className={className} />);
  const buttonElement = screen.getByTestId("buttonTest");
  expect(buttonElement).toHaveClass(className);
});

// Тест для проверки цвета фона кнопки
test("button has a given background color", () => {
  const backgroundColor = "red";
  render(<Button color={backgroundColor} />);
  const buttonElement = screen.getByTestId("buttonTest");
  expect(buttonElement).toHaveStyle({ backgroundColor });
});

test("onClick function is called when the button is clicked", () => {
  const onClickMock = jest.fn();
  render(<Button onClick={onClickMock} />);
  const buttonElement = screen.getByTestId("buttonTest");

  fireEvent.click(buttonElement);

  expect(onClickMock).toHaveBeenCalledTimes(1);
});
