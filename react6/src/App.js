import logo from "./logo.svg";
import "./App.css";
import { ProductList } from "./Componens/ProductList";
import { useSelector, useDispatch } from "react-redux";
import { useEffect, useState } from "react";
import { getProductAsync } from "./redux/actions/product";
import { Header } from "./Componens/Header";
import { AppRoutes } from "./Routes";
import { toggleListContext } from "./context/toggleListContex.js";
function App() {
  const [toggleList, setToggleList] = useState(false);
  return (
    <>
      <toggleListContext.Provider value={{ toggleList, setToggleList }}>
        <Header />
        <AppRoutes />
      </toggleListContext.Provider>
    </>
  );
}

export default App;
