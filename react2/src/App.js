import "./App.css";
import ProductList from "./Components/ProductList";
import Home from "./Home";

function App() {
  return (
    <div className="App">
      <Home />
    </div>
  );
}

export default App;
