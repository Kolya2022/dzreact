import React, { Component } from 'react';
import styles from './Modal.module.css';
import PropTypes from "prop-types";

export default class Modal extends Component {
    constructor(props) {
        super();


    }
    render() {
        const { text, id, header, actions, closeButton, modalRef, closeModal, onAddToCart } = this.props
        return (
            <div id={id} className={styles.Modal} onClick={closeModal} >
                <div className={styles.modalContent} ref={modalRef}>
                    <div className={styles.modalHeader}>{header} {closeButton ? <span className={`btn-close ${styles.buttonClose}`} >&#10761;</span> : null}</div>
                    <div className={styles.modalBody}>
                        <div>{text}</div>
                        <div className={styles.modalButtoms} onClick={onAddToCart}>{actions}</div>
                    </div>

                </div>
            </div>
        )
    }
}
Modal.propTypes = {
    header: PropTypes.string,
    text: PropTypes.string,
    actions: PropTypes.array,
    closeButton: PropTypes.bool,
    closeModal: PropTypes.func,
    onAddToCart: PropTypes.func,
    id: PropTypes.string,
};

Modal.defaultProps = {
    text: "Modal header",
    closeButton: true,


}