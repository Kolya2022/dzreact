import Button from "../Button"
import React from "react"

const configModal = [
    {
        id: '0',
        header: "Do you want to delete this file?",
        text: "Once you delete this file, it won’t be possible to undo this action. \n Are you sure you want to delete it?",
        closeButton: true,
        modalRef: React.createRef(),
        actions: [
            <Button text="Ok" backgroundColor="red" />,
            <Button text="Cancel" backgroundColor="red" />,
        ],


    },
    {
        id: '1',
        header: "Хотите добавиь товар в корзину",
        text: `Нажмите "ок" чтобы добавить`,
        closeButton: true,
        modalRef: React.createRef(),
        actions: [
            <Button text="ОК" backgroundColor="red" />,

        ],
    },
]

export default configModal