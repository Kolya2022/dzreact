import { Component } from 'react';
import styles from './Button.module.css';
import PropTypes from "prop-types";


export default class button extends Component {
    constructor(props) {
        super();
    }
    render() {
        const { onClick, text, backgroundColor, id } = this.props;
        return <button id={id} onClick={onClick} className={`Button ${styles.Button}`} style={{ backgroundColor }}>{text}</button>
    }

}

button.propTypes = {
    onClick: PropTypes.func,
    text: PropTypes.string,
    backgroundColor: PropTypes.string,
    id: PropTypes.string,

}
button.defaultProps = {
    backgroundColor: "red",
    text: "Button",

}  