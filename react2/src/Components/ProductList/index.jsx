import { Component } from "react"
import ProductCard from "../ProductCard";
import "./productcard.css";
import Modal from '../Modal';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import configModal from "../Modal/configmodal";



export default class ProductList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            cardInfo: [],
            isActive: false,
            modalInfo: {},
            numberElected: parseInt(localStorage.getItem('elected')) || 0,
            numberCards: parseInt(localStorage.getItem('basket')) || 0,
            faivorite: []
        }
    }

    componentDidMount() {
        const todo = JSON.parse(localStorage.getItem("cardList"))

        if (todo) {
            this.setState({
                cardInfo: [...todo]
            })
        } else {
            fetch("./config.json")
                .then((res) => res.json())
                .then((data) => {
                    this.setState({
                        cardInfo: [...data]
                    })
                })

        }


    }
    onStatusChange = (e) => {

        const id = e.target.id
        const currentcardInfo = this.state.cardInfo.find(
            (todo) => todo.id === id
        );
        if (!this.state.faivorite.includes(currentcardInfo.id)) {
            this.setState((state) => ({
                faivorite: [...state.faivorite, currentcardInfo.id],
            }));

        }
        (this.state.faivorite);

        (currentcardInfo);
        if (!currentcardInfo) {
            return;
        }

        const currentcardInfoIdx = this.state.cardInfo.findIndex(
            (card) => card.id === id
        );
        const updatedCard = {
            ...currentcardInfo,
            status: currentcardInfo.status === "not elected" ? "elected" : "not elected",
        };
        const newCardInfo = [
            ...this.state.cardInfo.slice(0, currentcardInfoIdx),
            updatedCard,
            ...this.state.cardInfo.slice(currentcardInfoIdx + 1),
        ];

        this.setState((state) => ({
            cardInfo: newCardInfo,
        }));

        if (updatedCard.status === 'elected') {
            const currentCount = parseInt(localStorage.getItem('elected')) || 0;
            const newCount = currentCount + 1;
            this.setState((prevState) => ({
                numberElected: prevState.numberElected + 1,
            }));
            localStorage.setItem('elected', newCount);
        } else {
            const currentCount = parseInt(localStorage.getItem('elected')) || 0;
            const newCount = currentCount - 1;
            this.setState((prevState) => ({
                numberElected: prevState.numberElected - 1,
            }));
            localStorage.setItem('elected', newCount);
        }
        localStorage.setItem("cardList", JSON.stringify(newCardInfo));


    };
    openModal = (e) => {
        const modalId = "1"
        const modal = configModal.find((modal) => modal.id === modalId);
        this.setState({
            modalInfo: {
                ...modal,
            },
            isActive: true,
        });
    };

    closeModal = (e) => {
        if (
            e.target.classList.contains("btn-close") ||
            e.target.classList.contains("Button")
        ) {
            this.setState({ isActive: false });
        }
    };
    handleAddToCart = (e) => {
        if (e.target.classList.contains('Button')) {
            const currentCount = parseInt(localStorage.getItem('basket')) || 0;
            const newCount = currentCount + 1;
            this.setState(prevState => ({
                numberCards: prevState.numberCards + 1

            }));
            localStorage.setItem('basket', newCount);
        }
    }

    render() {
        return (
            <>
                <div className="elected-product">
                    <span><FontAwesomeIcon icon={faShoppingCart} /> Количество товаров в корзине: {this.state.numberCards}</span>
                    <span>Количество избранных товаров: {this.state.numberElected}</span>
                </div>
                <div className="card-list">
                    {this.state.cardInfo.map((product) => {
                        return <ProductCard {...product} openModal={this.openModal} statusChange={this.onStatusChange} />
                    })}
                </div>
                {this.state.isActive ? (
                    <Modal closeModal={this.closeModal} {...this.state.modalInfo} onAddToCart={this.handleAddToCart} />
                ) : null}
            </>
        )
    }
}