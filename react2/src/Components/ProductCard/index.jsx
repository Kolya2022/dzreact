import { Component } from "react"
import "./card.css"
import PropTypes from "prop-types";
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faHeart } from '@fortawesome/free-solid-svg-icons';

export default class ProductCard extends Component {
    constructor(props) {
        super();
    }

    render() {
        const { name, price, color, articul, img, openModal, closeModal, id, status, statusChange, } = this.props

        return (
            <div className="product-card" onClick={closeModal} style={{ backgroundColor: color }} >
                <img className="card-img" src={img} alt="" />
                <p className="card-name">{name}</p>
                <p className="card-price">price:{price}</p>
                <p className="card-articul">articul:{articul}</p>
                <div className="container-botton">
                    <button id={id} onClick={openModal}>Add to cart</button>
                    <div onClick={statusChange} id={id} className={`heart ${status === "elected" ? "clicked" : ""}`} style={{ backgroundColor: status === "not elected" ? "yellow" : "red" }} ></div>

                </div>

            </div>
        )
    }
}

ProductCard.propTypes = {
    name: PropTypes.string,
    price: PropTypes.string,
    color: PropTypes.string,
    articul: PropTypes.string,
    img: PropTypes.string,
    openModal: PropTypes.func,
    closeModal: PropTypes.func,
    id: PropTypes.string,
    status: PropTypes.string,
    statusChange: PropTypes.func,
}
