import Button from "../button"
import React from "react"

const configModal = [
    {
        id: '0',
        header: "Do you want to delete this file?",
        text: "Once you delete this file, it won’t be possible to undo this action. \n Are you sure you want to delete it?",
        closeButton: true,
        modalRef: React.createRef(),
        actions: [
            <Button text="Ok" backgroundColor="red" />,
            <Button text="Cancel" backgroundColor="red" />,
        ],


    },
    {
        id: '1',
        header: "Are you sure want to exit?",
        text: `Press 'confirm' button to continue`,
        closeButton: true,
        modalRef: React.createRef(),
        actions: [
            <Button text="continue" backgroundColor="red" />,
            <Button text="Cancel" backgroundColor="red" />,
        ],
    },
]

export default configModal