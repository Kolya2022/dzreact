import React, { Component } from 'react';
import styles from './Modal.module.scss';


export default class Modal extends Component {
    constructor(props) {
        super();


    }
    render() {
        const { text, id, header, actions, closeButton, modalRef, onClick } = this.props
        return (
            <div id={id} className={styles.Modal} onClick={onClick} >
                <div className={styles.modalContent} ref={modalRef}>
                    <div className={styles.modalHeader}>{header} {closeButton ? <span className={`btn-close ${styles.buttonClose}`} >&#10761;</span> : null}</div>
                    <div className={styles.modalBody}>
                        <div>{text}</div>
                        <div className={styles.modalButtoms}>{actions}</div>
                    </div>

                </div>
            </div>
        )
    }
}
