import { Component } from 'react';
import styles from './Button.module.scss';


export default class button extends Component {
    constructor(props) {
        super();
    }
    render() {
        const { onClick, text, backgroundColor, id } = this.props;
        return <button id={id} onClick={onClick} className={styles.Button} style={{ backgroundColor }}>{text}</button>
    }

}