import "./App.css";
import Button from "./components/button";
import React, { Component } from "react";
import Modal from "./components/modal";
import configModal from "./components/configmodal";

class App extends Component {
  constructor(props) {
    super();
    this.state = {
      isActive: false,
      modalInfo: {},
    };
  }

  openModal = (e) => {
    const modalId = e.target.id;
    const modal = configModal.find((modal) => modal.id === modalId);
    this.setState({
      modalInfo: {
        ...modal,
      },
      isActive: true,
    });
  };

  closeModal = (e) => {
    e.target;
    if (
      e.target.classList.contains("btn-close") ||
      (this.state.modalInfo.modalRef.current &&
        !this.state.modalInfo.modalRef.current.contains(e.target))
    ) {
      this.setState({ isActive: false });
    }
  };

  render() {
    return (
      <>
        <div className="App">
          <div className="wrapper">
            <Button
              id={0}
              text="Open first modal"
              backgroundColor="white"
              onClick={this.openModal}
            />
            <Button
              id={1}
              text="Open second modal"
              backgroundColor="orange"
              onClick={this.openModal}
            />
          </div>
          {this.state.isActive ? (
            <Modal onClick={this.closeModal} {...this.state.modalInfo} />
          ) : null}
        </div>
      </>
    );
  }
}

export default App;
